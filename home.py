# -*- coding: utf-8 -*-
"""
Created on Tue Feb 23 16:42:00 2021

@author: user
"""


import KDC101

serial_no_L = 27504464 # serial number stage of laser

L_motor = KDC101.KDC_Motor(serial_no_L,"Laser") #create KDC motor object called laser

L_motor.go_home()
L_motor.mAbs(17.5) 

L_motor.close_Motor()


print("-----------------------------")
print("Finished Successfully!")
print("-----------------------------")