# -*- coding: utf-8 -*-
"""
BSC203 class to control the motors
using APTMotor from PyAPT 

PyAPT github:https://github.com/mcleu/PyAPT 
Einat
"""

# Import APTMotor class from PyAPT
from PyAPT.PyAPT import APTMotor

class BSC203:
    
    '''
    Initillaing BSC203 Object 
    '''
    
    def __init__(self,serial_no,name):
        # Create object corresponding to the motor.
        self.Motor = APTMotor(serial_no, HWTYPE =13) # The number should correspond to the serial number.
        # Use help APTMotor to obtain full list of hardware (HW) supported.
        self.name = name
        print("-----------------------------")
        print(f"BSC203 {self.name} Motor Connection Successful!")
        print("-----------------------------\n")
    
    '''
    BSC203 Methods
    '''
        
    def get_position(self):
        # return position in mm
        pos = self.Motor.getPos()
        print(f"Position of {self.name} is: {pos} mm")
        return self.Motor.getPos()
    
    def get_settings(self, P=True):
        # returns setting in float type
        minVel,acc,Vel = self.Motor.getVelocityParameters()
        if(P):
            print(f"Minimum Vel: {minVel} mm/s")
            print(f"Acceleration: {acc} mm/s/s")
            print(f"Velocity: {Vel} mm/s")
        settings = [minVel,acc,Vel]
        return settings  
    
    def get_settings_limits(self):
        # returns a list with the limit settings: max vel, max acc
        max_acc,max_vel = self.Motor.getVelocityParameterLimits()
        print(f"Maximum Accelration: {max_acc} mm/s/s")
        print(f"Maximum Velocity: {max_vel} mm/s")
        settings = [max_acc,max_vel]
        return settings
    
    def set_vel(self,vel):
        # sets the velocity [float] in mm/s
        print("Setting velocity:")
        self.Motor.setVel(vel)
        print("New Settings")
        self.get_settings()
        print("\n")
        
    def set_acc(self,acc):
        # set the accelration [float] in mm/s/s
        print("Setting accelration:")
        setting = self.get_settings(P=False)
        current_velocity = setting[2]
        self.Motor.setVelocityParameters(0, acc, current_velocity)
        print("New Settings")
        self.get_settings()
        print("\n")
        
            
    def close_Motor(self):
        # Clean up APT object, free up memory
        print(f"Closing {self.name} Motor...")
        self.Motor.cleanUpAPT()
     
    '''
    Controlling the motor
    m = move
    c = controlled velocity
    b = backlash correction

    Rel = relative distance from current position.
    Abs = absolute position
    '''   
    
    def mRel(self,rel_dis):
        # Moves the motor a relative distance specified rel_dis[float]
        # rel_dis can be float positive to advance and negative to retract 
        self.get_position()
        print(f"Moving {self.name} motor reltaive distance: {rel_dis} mm")
        self.Motor.mRel(rel_dis)
        print("Done!")
        self.get_position()
    
    def mAbs(self, abs_pos):
        # Moves the motor to the Absolute position specified
        # relative to home position 
        print(f"Moving {self.name} motor to absolute position: {abs_pos}")
        self.Motor.mAbs(abs_pos)
        print("Done!")
        self.get_position()
        
    def mcRel(self,rel_dis,vel):
        # Moves the motor a relative distance specified at a controlled velocity
        # defalut velocity is 0.5 mm/s
        # relDistance    float     Relative position desired
        # moveVel        float     Motor velocity, mm/sec
        print(f"Moving {self.name} motor relative distance {rel_dis}\
              mm in specified speed {vel} mm/s")
        self.Motor.mcRel(rel_dis,vel)
        print("Done!")
        self.get_position()
    
    def mcAbs(self,abs_pos,vel):
        # Moves the motor to the Absolute position specified at a controlled velocity
        # the absolute position is relative to home 
        # defalut velocity is 0.5 mm/s
        # absPosition    float     Position desired
        # moveVel        float     Motor velocity, mm/sec
        print(f"Moving {self.name} motor to absolute position: {abs_pos} at {vel} mm/s")
        self.Motor.mcAbs(abs_pos,vel)
        print("Done!")
        self.get_position()
        
    def go_home(self):
        # Move the stage to home position and reset position entry
        print(f"Moving {self.name} motor to home position")
        success = self.Motor.go_home()
        if(success):
            print(f"Motor {self.name} Homed!")
            self.get_position()
               
    
# stage1_x = BSC203(90160875)
# stage1_y = BSC203(90160876)
# stage1_x.get_position()
# stage1_y.get_position()
# stage1_y.go_home()
# # stage1_y.mRel(1)
# # stage1_y.mAbs(3)
# # stage1_x.get_settings()
# # stage1_x.set_acc(0.5)
# stage1_x.close_Motor()
# stage1_y.close_Motor()

