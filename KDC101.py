# -*- coding: utf-8 -*-
"""
KDC101 class to control the motors
using APTMotor from PyAPT 

PyAPT github:https://github.com/mcleu/PyAPT 
Einat
"""
# Import APTMotor class from PyAPT
from PyAPT.PyAPT import APTMotor
# import time

class KDC_Motor:
        
    def __init__(self,serial_no,name):
        # Create object corresponding to the motor.
        self.Motor = APTMotor(serial_no) # The number should correspond to the serial number.
        # Use help APTMotor to obtain full list of hardware (HW) supported.
        self.dis = 0.05 #distance to move when opening and closing the laser in mm
        self.name = name
        print("-----------------------------")
        print(f"KDC101 {self.name} Motor Connection Successful!")
        print("-----------------------------\n")

        
    def set_distance(self,dis):
        # setting the distance to move when opening and closing the laser
        self.dis = dis
        print(f"Setting the distance to open and close the laser to: {self.dis} mm")
        
        
    def set_vel(self,vel):
        # sets the velocity [float] in mm/s
        print("Setting velocity:")
        self.Motor.setVel(vel)
        print("New Settings")
        self.get_settings()
        print("\n")
        
    def set_acc(self,acc):
        # set the accelration [float] in mm/s/s
        print("Setting accelration:")
        setting = self.get_settings(P=False)
        current_velocity = setting[2]
        self.Motor.setVelocityParameters(0, acc, current_velocity)
        print("New Settings")
        self.get_settings()
        print("\n")
    
    def get_position(self):
        # return position in mm
        pos = self.Motor.getPos()
        print(f"Position of {self.name} is: {pos} mm")
        return self.Motor.getPos()
    
    def get_settings(self, P=True):
        # returns a list of the settings of the motors 
        # if P is true it also prints them
        minVel,acc,Vel = self.Motor.getVelocityParameters()
        if(P):
            print(f"Minimum Vel: {minVel} mm/s")
            print(f"Acceleration: {acc} mm/s/s")
            print(f"Velocity: {Vel} mm/s")
        settings = [minVel,acc,Vel]
        return settings
        
    def close_Motor(self):
        # closing the connection to the motor and cleans the memory 
        print(f"Closing {self.name} Motor...")
        self.Motor.cleanUpAPT()
        
    def close_laser(self):
        # moving the typical distance to close the laser
        # prints the position before and after closing the laser
        self.get_position()
        print("Closing Laser")
        self.Motor.mRel(-self.dis) # advance mm
        self.get_position()
        
    def open_laser(self):
        # moving the typical distance to open the laser 
        # prints the position before and after closing the laser
        self.get_position()
        print("Opening Laser")
        self.Motor.mRel(self.dis) # advance mm
        self.get_position()
        
    def go_home(self):
        # Move the stage to home position and reset position entry
        print(f"Moving {self.name} motor to home position")
        success = self.Motor.go_home()
        if(success):
            print(f"Motor {self.name} Homed!")
            self.get_position()
        
    def get_settings_limits(self):
        # returns a list of the settings limits - the max vel and acc
        # also prints these limits 
        max_acc,max_vel = self.Motor.getVelocityParameterLimits()
        print(f"Maximum Accelration: {max_acc} mm/s/s")
        print(f"Maximum Velocity: {max_vel} mm/s")
        settings = [max_acc,max_vel]
        return settings
    
    '''
    Controlling the motor
    m = move
    c = controlled velocity
    b = backlash correction

    Rel = relative distance from current position.
    Abs = absolute position
    '''   
    
    def mRel(self,rel_dis):
        # Moves the motor a relative distance specified rel_dis[float]
        # rel_dis can be float positive to advance and negative to retract 
        self.get_position()
        print(f"Moving {self.name} motor reltaive distance: {rel_dis} mm")
        self.Motor.mRel(rel_dis)
        print("Done!")
        self.get_position()
        
        
    def mAbs(self, abs_pos):
        # Moves the motor to the Absolute position specified
        # relative to home position 
        print(f"Moving {self.name} motor to absolute position: {abs_pos}")
        self.Motor.mAbs(abs_pos)
        print("Done!")
        self.get_position()
        
    def mcRel(self,rel_dis,vel):
        # Moves the motor a relative distance specified at a controlled velocity
        # defalut velocity is 0.5 mm/s
        # relDistance    float     Relative position desired
        # moveVel        float     Motor velocity, mm/sec
        print(f"Moving {self.name} motor relative distance {rel_dis}\
              mm in specified speed {vel} mm/s")
        self.Motor.mcRel(rel_dis,vel)
        print("Done!")
        self.get_position()
    
    def mcAbs(self,abs_pos,vel):
        # Moves the motor to the Absolute position specified at a controlled velocity
        # the absolute position is relative to home 
        # defalut velocity is 0.5 mm/s
        # absPosition    float     Position desired
        # moveVel        float     Motor velocity, mm/sec
        print(f"Moving {self.name} motor to absolute position: {abs_pos} at {vel} mm/s")
        self.Motor.mcAbs(abs_pos,vel)
        print("Done!")
        self.get_position()
        


### Move motor forward by 1mm, wait half a second, and return to original position.
### mRel is move relative. mAbs is move absolute (go to position xxx)
#Motor1.mRel(0.5) # advance 1mm
#time.sleep(.5)
##print("after 1 mm",Motor1.getPos())
##Motor1.mRel(-1) # retract 1mm
##
##time.sleep(1)
##
### Move motor forward by 1mm, wait half a second, and return to original position, at a velocity of 0.5mm/sec
##motVel = 0.5 #motor velocity, in mm/sec
##Motor1.mcRel(1, motVel) # advance 1mm
##time.sleep(.5)
##Motor1.mcRel(-1, motVel) # retract 1mm
##
#
# Clean up APT object, free up memory
#Motor1.cleanUpAPT()


# m=KDC_Motor(27504464)
# m.get_position()
# m.get_settings()
# # m.get_settings_limits()

# # m.open_laser()
# # m.get_position()
# # m.go_home()
# # m.get_position()
# m.close_Motor()