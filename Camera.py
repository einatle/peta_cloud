# -*- coding: utf-8 -*-
"""
Created on Thu Feb 11 11:10:49 2021

@author: user
"""

from instrumental import instrument, list_instruments
import matplotlib.pyplot as plt
from matplotlib import cm

class Camera:
    
    def __init__(self,cam_name):
        self.camera = instrument(cam_name)
        
    def close(self):
        self.camera.close()

c = Camera('cam_4103485830')

c.close()
        
        