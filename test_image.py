# -*- coding: utf-8 -*-
"""
Einat
"""


from instrumental import instrument, list_instruments
import matplotlib.pyplot as plt
from matplotlib import cm
import time



# HOW TO TAKE AN IMAGE
cam = instrument('cam_4103465499')
cam.gain_boost=True
img = cam.grab_image()
print("getting second image")
img = cam.grab_image()

# plt.imshow(img, cmap='gray', vmin=0, vmax=5)
plt.imshow(img, cmap='gray', vmin=0, vmax=5)
plt.xlabel('X Pixel')
plt.ylabel('Y Pixel')
# plt.show()
plt.savefig('image1502.png',dpi=500) 

cam.close()

del cam

