# -*- coding: utf-8 -*-
"""
Main code to operate the system

Einat
"""

import KDC101 #red cube
import BSC203 #big stage
import time
# import DC_camera


'''
Open Close Laser parameters:

    serial_no_L - this is the serial number of the stage that is used to open 
                  and close the motor
                  
    open_L_distance - this is the distance the the motor will move when calling
                      the funciton open/close laser. 
    
    motor_L_velocity - this defines the motor velocity. 
    
    motor_L_acc - this defines the motor accelertaion. 
    
Information on Serial Numbers:
    
CUBE1 -> SN: 27254412
CUBE2 -> SN: 27504464 

'''


serial_no_L = 27504464 # serial number stage of laser
open_L_distance = 6 #mm
motor_L_velocity = 2.39 #mm/s
motor_L_acc = 4.5 #mm/s/s


'''
Glass motion parameter (stage1)

    serial_no_G - this is the serial number of the stage that holds the glass
    
    move_G_distance - the distance that the glass needs to move
    
    move_G_time - the time frame to move the glass distance
    
    motor_G_acc - the acc ot the glass motor
    
    motor_G_velocity - the veloicty of the glass motor is calculated by 
                       solving the 2nd deg eq. 

Information on Serial Numbers:
    
Stage 1 (glass)
    - SN: 90160876 y axis 
    - SN: 90160875 x axis
    - SN: 90160877 z axis
    
Stage 2 (fiber)
    - SN: 90160925 x axis 
    - SN: 90160926 y axis
    - SN: 90160927 z axis
'''



# =============================================================================
# Main
# =============================================================================


L_motor = KDC101.KDC_Motor(serial_no_L,"Laser") #create KDC motor object called laser

# set all the parameters: vel, acc
print("-----------------------------")
print("LASER SETTINGS")
print("-----------------------------")
L_motor.set_distance(open_L_distance)
L_motor.set_vel(motor_L_velocity)
L_motor.set_acc(motor_L_acc)



#open laser                                                                                           
# L_motor.open_laser()


#close laser
L_motor.close_laser()




L_motor.close_Motor()

print("-----------------------------")
print("Finished Successfully!")
print("-----------------------------")
