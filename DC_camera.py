# -*- coding: utf-8 -*-
"""
Einat
"""

from instrumental import instrument, list_instruments
import matplotlib.pyplot as plt
from matplotlib import cm
from datetime import datetime
import os
import cv2
import glob


class DC_camera:
    
    def __init__(self,camera_name,path=r"C:\Users\user\Pictures"):
        self.cam = instrument(camera_name)
        self.path = path
        self.img_arr = []
        self.number_of_frames = 80

    def save_image(self,path = None):
        self.cam.gain_boost=True
        img = self.cam.grab_image()
        img = self.cam.grab_image()
        plt.imshow(img, cmap='gray', vmin=0, vmax=5)
        # plt.xlabel('X Pixel')
        # plt.ylabel('Y Pixel')
        # plt.show()
        if(path!=None):
            plt.savefig(path ,dpi=500) 
            print(f"Saving image to: {path}")
        else:
            path_to_save = self.path + '\Image___'+ \
                datetime.now().strftime("%d_%m_%Y__%H_%M_%S")+'.png'
            plt.savefig(path_to_save ,dpi=500) 
            print(f"Saving image to: {path_to_save}")
        return img
    
    def save_video(self, path = None):
        # first frames are captured from camera to an array img_arr
        # next all of the images are save to a folder as png
        # finally a video is generated from the images
        print("Taking Video")
        self.cam.gain_boost=True
        i=1
        self.img_arr = []
        while(i<=self.number_of_frames):
            self.cam.start_live_video()
            
            self.cam.wait_for_frame()
            self.img_arr.append(self.cam.latest_frame()) 
            print(f"Still {i}")
            self.cam.stop_live_video()
            i+=1 
        
        if(path!=None):  
            path_to_save = path
        else:
            path_to_save = self.path + '\\' + datetime.now().strftime("%d_%m_%Y__%H_%M_%S")
            print(f"Saving images to {path_to_save}")
        
        os.mkdir(path_to_save)
        for i in range(len(self.img_arr)):
            plt.imshow(self.img_arr[i], cmap='gray', vmin=0, vmax=5)   
            plt.savefig((path_to_save + '\\' + 'image%02d___'+ datetime.now().strftime("%d_%m_%Y__%H_%M_%S"))% i,dpi=500)
            # plt.imsave('Image%02d.png', img_arr[i], cmap='gray', vmin=0, vmax=5)
            print(f'Creating png {i}')
            # plt.clf()
            plt.close()
        
        png_arr = []
        for filename in glob.glob(path_to_save + '\*.png'):
            img = cv2.imread(filename)
            height, width, layers = img.shape
            size = (width,height)
            png_arr.append(img)
    
    
        out = cv2.VideoWriter(path_to_save + '\Video___'+ datetime.now().strftime("%d_%m_%Y__%H_%M_%S")+'.avi',cv2.VideoWriter_fourcc(*'DIVX'), 15, size)
         
        for i in range(len(png_arr)):
            out.write(png_arr[i])
        out.release()
        print(f"Video Created at: {path_to_save}")
        
    def close(self):
        self.cam.close()
        del self.cam


# cam = DC_camera('cam_4103485830')
# print("camera opened")
# image = cam.save_image()
# cam.save_video()
# cam.close()




# =============================================================================
# IGNORE FOR NOW
# from instrumental import drivers
# from instrumental.drivers.cameras import uc480
# import os
# camera_list = uc480.list_instruments()
# print(dict(camera_list[0]))
# cam_id = 1
# cam_serial_number = '4103465499'
# 
# if __name__ == '__main__':
#     
#     cam = uc480.UC480_Camera('cam_4103465499')
#     # # print(cam)
#     # # cam = instrument('cam_4103465499')
#     # # cam.save_instrument('cam_4103465499')
#     
#     # img = cam.grab_image()
#     
#     cam.close(force=True)
# 
# 
# 
# =============================================================================
