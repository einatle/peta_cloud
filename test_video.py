# -*- coding: utf-8 -*-
"""
Created on Thu Feb 11 13:46:20 2021

@author: user

https://stackoverflow.com/questions/34975972/how-can-i-make-a-video-from-array-of-images-in-matplotlib
"""

import numpy as np
import matplotlib.pyplot as plt
import os
from instrumental import instrument, list_instruments
import matplotlib.image as mpimg
import subprocess
import cv2
import glob
from datetime import datetime




cam=instrument('cam_4103485830')
cam.gain_boost=True
img_arr = []
i=0
number_of_frames=100

while i<=number_of_frames:
    cam.start_live_video()
    
    cam.wait_for_frame()
    img_arr.append(cam.latest_frame()) 
    print(f"click {i}")
    
    # plt.imshow(img_arr[temp], cmap='gray', vmin=0, vmax=5)
    # plt.xlabel('X Pixel')
    # plt.ylabel('Y Pixel')
    # plt.show()    
    
    cam.stop_live_video()
    i+=1

def generate_folder_photos(img_arr):
    # img_arr is an array containg 2D numpy arrays of the images taken
    # first creating multiple png images from the frames of the captured video
    path = r"C:\Users\user\Pictures"
    path = path + '\\' + datetime.now().strftime("%d_%m_%Y__%H_%M_%S")
    
    os.mkdir(path)
    print("saving the figures")
    for i in range(len(img_arr)):
        plt.imshow(img_arr[i], cmap='gray', vmin=0, vmax=5)   
        plt.savefig((path + '\\' + 'image%02d___'+ datetime.now().strftime("%d_%m_%Y__%H_%M_%S"))% i,dpi=500)
        # plt.imsave('Image%02d.png', img_arr[i], cmap='gray', vmin=0, vmax=5)
        print(f'Fig {i}')
        # plt.clf()
        plt.close()
        
    return path


def create_video_from_img(path):
    png_arr = []
    for filename in glob.glob(path + '\*.png'):
        img = cv2.imread(filename)
        height, width, layers = img.shape
        size = (width,height)
        png_arr.append(img)
    
    
    out = cv2.VideoWriter(path+'\project.avi',cv2.VideoWriter_fourcc(*'DIVX'), 15, size)
     
    for i in range(len(png_arr)):
        out.write(png_arr[i])
    out.release()

path = generate_folder_photos(img_arr)
create_video_from_img(path)
cam.close()
del cam






# =============================================================================
# 
# import numpy as np
# import matplotlib.pyplot as plt
# import time
# import os
# import glob
# import shutil
# 
# from instrumental import instrument, list_instruments
# import matplotlib
# import matplotlib.image as mpimg
# from scipy import misc
# 
# plt.ion()
# 
# time_stop=time.time()
# print('Running...')
# 
# cam=instrument('cam_4103485830')
# 
# temp=1
# NL=10
# while temp<=NL:
#     cam.start_live_video()
#     
#     cam.wait_for_frame()
#     arr = cam.latest_frame()
#     print(temp)
#     
#     
#     fig=plt.figure(2)
#     plt.subplot(211)
#     plt.axis([0,800,0,255])
#     iplot=plt.plot(arr)
#     plt.show()
#     
#     
#     plt.subplot(212)
#     imgplot=plt.imshow(arr)
#     plt.pause(1)
#     plt.show()
#     plt.clf()
#     
#     cam.stop_live_video()
# =============================================================================
