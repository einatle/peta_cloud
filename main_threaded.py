# -*- coding: utf-8 -*-
"""
Main code to operate the system

Einat
"""

import KDC101
import BSC203
import time
import threading
'''
open close laser parameters
'''
serial_no_L = 27504464
open_L_distance = 3 #mm
motor_L_velocity = 2.39 #mm/s
motor_L_acc = 4.5 #mm/s/s

'''
glass motion parameter (stage1)
'''
serial_no_G = 90160926
move_G_distance = 5 #mm
move_G_time = 0.3 #sec
# motor_G_velocity = 2.4 #mm/s
motor_G_acc = 5 #mm/s/s

start_delay = 3
end_delay = 3 

# Calculate G velocity - solving linar equation 
# v1 = (-move_G_time + (move_G_time**2+4*(move_G_distance/motor_G_acc))**0.5)/(-2/motor_G_acc)
a =-1/motor_G_acc
b =move_G_time
c =-move_G_distance
# v2 = (-b + (b**2-4*(a*c))**0.5)/(2*a)
v2 = 2.4


def start_threading():
        thread_G = threading.Thread(target=move_G_motor)
        thread_G.daemon = True
        thread_L = threading.Thread(target=move_L_motor)
        thread_L.daemon = True
        thread_L.start()
        thread_G.start()


def move_G_motor():
    G_motor_y.mRel(move_G_distance)
    G_motor_y.close_Motor()


def move_L_motor():
    L_motor.open_laser()
    # time.sleep(start_delay)
    L_motor.close_laser()
    L_motor.close_Motor()



# =============================================================================
# Main
# =============================================================================

L_motor = KDC101.KDC_Motor(serial_no_L,"Laser")


# set all the parameters: vel, acc
print("-----------------------------")
print("LASER SETTINGS")
print("-----------------------------")
L_motor.set_distance(open_L_distance)
L_motor.set_vel(motor_L_velocity)
L_motor.set_acc(motor_L_acc)



G_motor_y = BSC203.BSC203(serial_no_G,"Glass")
print("-----------------------------")
print("GLASS SETTINGS")
print("-----------------------------")
G_motor_y.set_acc(motor_G_acc)
G_motor_y.set_vel(v2)


start = input("start motion?")
if(start):
    start_threading()

print("-----------------------------")
print("Motion Begin")
print("-----------------------------")



