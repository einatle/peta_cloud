# -*- coding: utf-8 -*-
"""
Main code to operate the system

Einat
"""

import KDC101 #red cube
import BSC203 #big stage
import time
# import DC_camera


'''
Open Close Laser parameters:

    serial_no_L - this is the serial number of the stage that is used to open 
                  and close the motor
                  
    open_L_distance - this is the distance the the motor will move when calling
                      the funciton open/close laser. 
    
    motor_L_velocity - this defines the motor velocity. 
    
    motor_L_acc - this defines the motor accelertaion. 
    
Information on Serial Numbers:
    
CUBE1 -> SN: 27254412
CUBE2 -> SN: 27504464 

'''


serial_no_L = 27504464 # serial number stage of laser
open_L_distance = 6 #mm
motor_L_velocity = 2.39 #mm/s
motor_L_acc = 4.5 #mm/s/s


'''
Glass motion parameter (stage1)

    serial_no_G - this is the serial number of the stage that holds the glass
    
    move_G_distance - the distance that the glass needs to move
    
    move_G_time - the time frame to move the glass distance
    
    motor_G_acc - the acc ot the glass motor
    
    motor_G_velocity - the veloicty of the glass motor is calculated by 
                       solving the 2nd deg eq. 

Information on Serial Numbers:
    
Stage 1 (glass)
    - SN: 90160876 y axis 
    - SN: 90160875 x axis
    - SN: 90160877 z axis
    
Stage 2 (fiber)
    - SN: 90160925 x axis 
    - SN: 90160926 y axis
    - SN: 90160927 z axis
'''


serial_no_G = 90160876 #y axis of  stage 1 - glass

move_G_distance = 0.04 #mm
move_G_time = 1 #sec
# motor_G_velocity = 2.39 #mm/s
motor_G_acc = 5 #mm/s/s

start_delay = 3 #delay after sending command to open laser, to allow full openning. in sec
end_delay = 3 #delay after glass motion has ended and before sending command to colose laser

# =============================================================================
# Main
# =============================================================================

# Calculate G velocity - solving 2nd deg equation 

# v1 = (-move_G_time + (move_G_time**2+4*(move_G_distance/motor_G_acc))**0.5)/(-2/motor_G_acc) #small velovity result square equation
a =-1/motor_G_acc
b =move_G_time
c =-abs(move_G_distance)
motor_G_velocity = (-b + (b**2-4*(a*c))**0.5)/(2*a) #large velocity result square equation

L_motor = KDC101.KDC_Motor(serial_no_L,"Laser") #create KDC motor object called laser
G_motor_y = BSC203.BSC203(serial_no_G,"Glass")#create bsc motor object called glass

# set all the parameters: vel, acc
print("-----------------------------")
print("LASER SETTINGS")
print("-----------------------------")
L_motor.set_distance(open_L_distance)
L_motor.set_vel(motor_L_velocity)
L_motor.set_acc(motor_L_acc)


print("-----------------------------")
print("GLASS SETTINGS")
print("-----------------------------")
G_motor_y.set_acc(motor_G_acc)
G_motor_y.set_vel(motor_G_velocity)#distance is defined in move relative command (later on)


print("-----------------------------")
print("Motion Begin")
print("-----------------------------")

                                                                                            
t1 = time.time()
L_motor.open_laser()
t2 = time.time() - t1
print(f"laser closing motion took: {t2} sec")

time.sleep(start_delay)# to allow full openning of the laser

t1 = time.time()
G_motor_y.mRel(move_G_distance)
t2 = time.time() - t1
print(f"glass motion took: {t2} sec")
time.sleep(end_delay)
L_motor.close_laser()

L_motor.close_Motor()
G_motor_y.close_Motor()

print("-----------------------------")
print("Finished Successfully!")
print("-----------------------------")
