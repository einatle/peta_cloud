# -*- coding: utf-8 -*-
"""
Created on Mon Feb  8 10:22:05 2021

@author: user
"""

import logging
import threading
import time
import BSC203
import KDC101

serial_no_L = 27504464
open_L_distance = 3 #mm
motor_L_velocity = 2.39 #mm/s
motor_L_acc = 4.5 #mm/s/s

serial_no_G = 90160876
move_G_distance = 0.05 #mm
move_G_time = 0.3 #sec
# motor_G_velocity = 2.4 #mm/s
motor_G_acc = 5 #mm/s/s

start_delay = 3
end_delay = 3 


def x(name):
    logging.info("Thread %s: starting", name)
    L_motor.open_laser()
    time.sleep(start_delay)
    logging.info("Thread %s: finishing", name)
    L_motor.close_laser()
    L_motor.close_Motor()
    
def y(name):
    logging.info("Thread %s: starting", name)
    G_motor_y.mRel(move_G_distance)
    logging.info("Thread %s: finishing", name)
    G_motor_y.close_Motor()


if __name__ == "__main__":
    format = "%(asctime)s: %(message)s"
    logging.basicConfig(format=format, level=logging.INFO,
                        datefmt="%H:%M:%S")
    
    L_motor = KDC101.KDC_Motor(serial_no_L,"Laser")
    G_motor_y = BSC203.BSC203(serial_no_G,"Glass")
    
    logging.info("Main    : before creating thread")
    x = threading.Thread(target=x, args=('x',),daemon=True)
    y = threading.Thread(target=y, args=('y',),daemon=True)
    logging.info("Main    : before running thread")
    x.start()
    y.start()
    logging.info("Main    : wait for the thread to finish")
    # x.join()
    logging.info("Main    : all done")

