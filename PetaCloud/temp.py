import clr
import os
import pdb

bin_path = r'C:\Program Files\Thorlabs\Scientific Imaging\DCx Camera Support\Develop\DotNet\signed'
driver_path = r'C:\Users\user\Desktop\Project\PetaCloud\DCx_Camera_Interfaces_2018_09\DCx_Camera_Drivers\64'

os.environ['PATH'] += os.pathsep + bin_path + os.pathsep + driver_path
# print(os.path.join(bin_path, 'uc480DotNet.dll'))
clr.AddReference('uc480DotNet')
from uc480 import Camera, Defines, Info, Types
from ctypes import c_int, pointer
import inspect
import pdb


# get list of all possible status, comment out as needed
# print(dir(Defines.Status))

# create camera
cam = Camera()
cam_list = list()
# print all camera methods
print(dir(cam))
print("\n")
print(dir(cam.Device))
print("\n")
print(dir(cam.Device.GetDeviceID))
print("\n")



# Info.Camera.GetCameraList(Types.CameraInformation)

print("\n")


print(dir(Types.CameraInformation))
print(Types.CameraInformation().CameraID)
print(Types.CameraInformation().SerialNumber)

a= Types.CameraInformation()
obj = Info.Camera.GetCameraList(a)

# # init first camera
# status = cam.Init(0)
# if status != Defines.Status.SUCCESS:
#     ValueError(f'Got error code {status}')



# # # # Set display mode to bitmap (DiB)
# # # cam.Display.Mode.Set(Defines.DisplayMode.DiB)

# # # # Set color mode to 8-bit RGB
# # # cam.PixelFormat.Set(Defines.ColorMode.RGBA8Packed)

# # # # Set trigger mode to software (single image acquisition)
# # # cam.Trigger.Set(Defines.TriggerMode.Software)

# # # # Allocate image memory
# # # mem_id = cam.Memory.Allocate(True)

# # # # # Obtain image information
# # # # w, h, pixel_depth, pitch = cam.Memory.Inquire(mem_id)
# # # # Acquire image
# # # cam.Acquisition.Freeze(Defines.DeviceParameter.Wait)
# # # # Copy image from memory
# # # buffer = cam.Memory.CopyToArray(mem_id)

# # Close camera
# cam.Exit()
